/*
** list.c for fifo in /home/chartier/Programming/fifo_double_linked
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Fri Apr 13 16:29:54 2018 CHARTIER Rodolphe
** Last update Fri Apr 13 16:29:55 2018 CHARTIER Rodolphe
*/

#include "list.h"

void		list_init(t_list *list)
{
	list->size = 0;
	list->first = NULL;
	list->last = NULL;
}

int			list_add(t_list *list, void *data)
{
	t_elem	*elem;

	if ((elem = malloc(sizeof(*elem))) == NULL)
		return (-1);
	elem->data = data;
	elem->prev = list->last ? list->last : NULL;
	elem->next = NULL;
	++list->size;
	if (list->first == NULL)
		list->first = elem;
	if (list->last)
		list->last->next = elem;
	list->last = elem;
	return (0);
}

void		list_del(t_list *list, t_elem *elem, void (*free_data)(void *))
{
	t_elem	*tmp;

	tmp = list->first;
	while (tmp)
	{
		if (tmp == elem)
		{
			if (tmp->prev)
				tmp->prev->next = tmp->next;
			if (tmp->next)
				tmp->next->prev = tmp->prev;
			if (tmp == list->first)
				list->first = tmp->next;
			if (tmp == list->last)
				list->last = tmp->prev;
			--list->size;
			if (free_data)
				free_data(tmp->data);
			free(tmp);
			return ;
		}
		tmp = tmp->next;
	}
}

t_elem		*list_get(t_list *list, void *data, int (*cmp)(void *, void *))
{
	t_elem	*elem;

	elem = list->first;
	while (elem)
    {
		if (cmp)
		{
			if (!cmp(elem->data, data))
				return (elem);
		}
		else if (elem->data == data)
			return (elem);
		elem = elem->next;
    }
	return (NULL);
}

void		list_apply(t_list *list, void (*func)(void *))
{
	t_elem	*elem;

	if (!func)
		return ;
	elem = list->first;
	while (elem)
	{
		func(elem->data);
		elem = elem->next;
    }
}

void		list_free(t_list *list, void (*free_data)(void *))
{
	t_elem	*elem;
	t_elem	*next;

	elem = list->first;
	while (elem)
    {
		next = elem->next;
		if (free_data)
			free_data(elem->data);
		free(elem);
		elem = next;
    }
	list_init(list);
}

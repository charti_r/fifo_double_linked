/*
** main.c for fifo in /home/chartier/Programming/fifo_double_linked
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Fri Apr 13 16:29:43 2018 CHARTIER Rodolphe
** Last update Sun Apr 15 11:54:34 2018 CHARTIER Rodolphe
*/

#include <stdlib.h>
#include <stdio.h>

#include "list.h"

static void	print_elem(int nb)
{
	printf("%d\n", nb);
}

int				main(void)
{
	t_list	list;
	int			i;

	list_init(&list);
	i = -1;
	while (++i < 20)
		list_add(&list, i);

	list_del(&list, list_get(&list, 10, NULL), NULL);
	list_apply(&list, &print_elem);
	printf("\n");
	list_del(&list, list_get(&list, 0, NULL), NULL);
	list_del(&list, list_get(&list, 19, NULL), NULL);
	list_apply(&list, &print_elem);
	printf("\n");
	list_free(&list, NULL);
	return (0);
}

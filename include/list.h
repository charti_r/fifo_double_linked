/*
** list.h for fifo in /home/chartier/Programming/fifo_double_linked
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Fri Apr 13 16:29:18 2018 CHARTIER Rodolphe
// Last update Sun Apr 15 11:52:58 2018 CHARTIER Rodolphe
*/

#ifndef LIST_H_
# define LIST_H_

# include <stdlib.h>

typedef struct		s_elem
{
	void			*data;
	struct s_elem	*prev;
	struct s_elem	*next;
}					t_elem;

typedef struct		s_list
{
	size_t			size;
	t_elem			*first;
	t_elem			*last;
}					t_list;

#endif /* !LIST_H_ */
